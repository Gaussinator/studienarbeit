import { Component, OnInit } from '@angular/core';

import { TodoModel } from '../shared/todo-model';
import { TodoService } from '../shared/todo.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-edit-todo-list',
  templateUrl: './edit-todo-list.component.html',
  styleUrls: ['./edit-todo-list.component.scss']
})
export class EditTodoListComponent implements OnInit {
  public todoList: TodoModel[];
  private todoListSubscription: Subscription;

  constructor(public todoService: TodoService) {}

  ngOnInit() {
    this.todoListSubscription = this.todoService
      .getTodoListSubject()
      .subscribe(todoList => {
        this.todoList = todoList;
      });
    this.todoService.loadTodos();
  }

  public removeTodo(todo: TodoModel): void {
    this.todoService.removeTodo(todo.id).subscribe(
      response => {
        this.todoService.loadTodos();
      },
      error => {
        console.log(error);
        this.todoService.loadTodos();
      }
    );
  }
}
