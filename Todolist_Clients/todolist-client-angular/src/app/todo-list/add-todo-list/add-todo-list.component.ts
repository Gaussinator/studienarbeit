import { TodoService } from './../shared/todo.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-add-todo-list',
  templateUrl: './add-todo-list.component.html',
  styleUrls: ['./add-todo-list.component.scss']
})
export class AddTodoListComponent implements OnInit {
  constructor(public todoService: TodoService) {}

  ngOnInit() {}

  public addTodo(todo: string) {
    if (todo && (todo !== '' || todo !== undefined)) {
      this.todoService.addTodoToTodoList(todo).subscribe(response => {
        this.todoService.loadTodos();
      });
    }
  }
}
