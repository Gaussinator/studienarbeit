import { BehaviorSubject, Observable, Subscription, timer } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable, OnDestroy } from '@angular/core';

import { TodoModel } from './todo-model';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};

const serverUrl = 'http://localhost:8080/';

@Injectable({
  providedIn: 'root'
})
export class TodoService implements OnDestroy {
  private reloadTodoSubscription: Subscription;

  private errorOnRequest: boolean;
  private errorOnRequestSubject: BehaviorSubject<boolean> = new BehaviorSubject<
    boolean
  >(false);

  private todoList: TodoModel[];
  private todoListSubject: BehaviorSubject<TodoModel[]> = new BehaviorSubject<
    TodoModel[]
  >(this.todoList);

  constructor(private http: HttpClient) {
    this.reloadTodoSubscription = timer(0, 3000)
      .pipe(switchMap(() => this.getCompleteTodoList()))
      .subscribe(todoList => this.loadTodos());
  }

  ngOnDestroy() {
    this.reloadTodoSubscription.unsubscribe();
  }

  public getTodoListSubject(): BehaviorSubject<TodoModel[]> {
    return this.todoListSubject;
  }

  public getErrorOnRequestSubject(): BehaviorSubject<boolean> {
    return this.errorOnRequestSubject;
  }

  private setErrorOnRequest(errorHasOccurred: boolean): void {
    this.errorOnRequest = errorHasOccurred;
    this.errorOnRequestSubject.next(this.errorOnRequest);
  }

  public getCompleteTodoList(): Observable<TodoModel[]> {
    return this.http.get(`${serverUrl}api/todo/getCompleteList`).pipe(
      map(
        todoList => Object.values(todoList).map(todo => new TodoModel(todo)),
        catchError((error, caught) => {
          this.setErrorOnRequest(true);
          throw caught;
        })
      )
    );
  }

  public loadTodos(): void {
    this.getCompleteTodoList().subscribe(
      todoList => {
        this.todoList = todoList;
        this.todoListSubject.next(this.todoList);
        this.setErrorOnRequest(false);
      },
      error => {
        this.setErrorOnRequest(true);
      }
    );
  }

  public addTodoToTodoList(todo: string): Observable<any> {
    return this.http.post(`${serverUrl}api/todo/add`, todo, httpOptions).pipe(
      map(response => this.setErrorOnRequest(false)),
      catchError((error, caught) => {
        this.setErrorOnRequest(true);
        throw caught;
      })
    );
  }

  public removeTodo(id: number): Observable<any> {
    const url = `${serverUrl}api/todo/delete/${id}`;

    return this.http.delete(url, httpOptions).pipe(
      map(reponse => this.setErrorOnRequest(false)),
      catchError((error, caught) => {
        this.setErrorOnRequest(true);
        throw caught;
      })
    );
  }
}
