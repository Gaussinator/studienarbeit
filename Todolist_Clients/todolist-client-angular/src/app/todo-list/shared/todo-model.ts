export class TodoModel {
  public id: number = undefined;
  public description: string = undefined;

  constructor(kwargs?: any) {
    if (!kwargs) {
      return;
    }
    for (const key in this) {
      if (kwargs.hasOwnProperty(key)) {
        this[key] = kwargs[key];
      }
    }
  }
}
