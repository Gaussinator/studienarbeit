import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AddTodoListComponent } from './todo-list/add-todo-list/add-todo-list.component';
import { EditTodoListComponent } from './todo-list/edit-todo-list/edit-todo-list.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { HttpClientModule } from '@angular/common/http';
import { ErrorMessageComponent } from './todo-list/error-message/error-message.component';

@NgModule({
  declarations: [
    AppComponent,
    AddTodoListComponent,
    EditTodoListComponent,
    DashboardComponent,
    ErrorMessageComponent
  ],
  imports: [BrowserModule, AppRoutingModule, HttpClientModule],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
