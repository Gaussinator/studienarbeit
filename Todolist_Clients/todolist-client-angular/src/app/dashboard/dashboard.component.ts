import { Subscription } from 'rxjs';
import { Component, OnInit } from '@angular/core';
import { TodoService } from '../todo-list/shared/todo.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  public errorOnRequest: boolean;
  private errorOnRequestSubscription: Subscription;

  constructor(private todoService: TodoService) {
    this.errorOnRequestSubscription = this.todoService
      .getErrorOnRequestSubject()
      .subscribe(errorOnRequest => {
        this.errorOnRequest = errorOnRequest;
      });
  }

  ngOnInit() {}
}
