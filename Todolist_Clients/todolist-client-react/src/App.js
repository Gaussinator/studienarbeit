import React, { Component } from 'react';
import RequestService from './requestService';
import AddTodo from './components/addTodo';
import EditTodo from './components/editTodo';
import 'bootstrap/dist/css/bootstrap.css';
import { Jumbotron } from 'react-bootstrap';
import './App.css';

const requestService = new RequestService('http://localhost:8080/');

class App extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      todos: [],
      errorOnRequest: false
    };
    this.loadCompleteTodoList();
  }

  reloadTodos() {
    this.loadCompleteTodoList();
  }

  componentDidMount() {
    this.interval = setInterval(() => this.reloadTodos(), 3000);
  }

  componentWillUnmount() {
    clearInterval(this.interval);
  }

  renderAddTodo() {
    return <AddTodo />;
  }

  addTodoToTodoList(todo) {
    requestService
      .addTodoToTodoList(todo)
      .then(res => {
        this.loadCompleteTodoList();
        this.setState({ errorOnRequest: false });
      })
      .catch(error => {
        this.state.errorOnRequest = true;
      });
  }

  removeTodoFromTodoList(todoId) {
    requestService
      .removeTodo(todoId)
      .then(res => {
        this.loadCompleteTodoList();
        this.setState({ errorOnRequest: false });
      })
      .catch(error => {
        this.setState({ errorOnRequest: true });
        this.loadCompleteTodoList();
      });
  }

  loadCompleteTodoList() {
    requestService
      .getCompleteTodoList()
      .then(res => {
        this.setState({ todos: res.data, errorOnRequest: false });
      })
      .catch(error => {
        this.setState({ errorOnRequest: true });
      });
  }

  render() {
    return (
      <div>
        <link
          rel="stylesheet"
          href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css"
          integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp"
          crossOrigin="anonymous"
        />
        <Jumbotron className="jumbotron">
          <div className="container">
            <h1>React.js To-Do-List Application</h1>
            <p>Add, remove or watch your to-do's</p>
          </div>
        </Jumbotron>
        <div className="container">
          <AddTodo onClick={todo => this.addTodoToTodoList(todo)} />
          <EditTodo
            todos={this.state.todos}
            errorOnRequest={this.state.errorOnRequest}
            onClick={todoId => this.removeTodoFromTodoList(todoId)}
          />
        </div>
      </div>
    );
  }
}
export default App;
