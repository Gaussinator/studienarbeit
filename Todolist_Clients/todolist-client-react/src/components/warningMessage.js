import React from 'react';
import { Alert } from 'react-bootstrap';
import './warningMessage.css';

export default class WarningMessage extends React.Component {
  constructor(props, context) {
    super(props, context);
  }

  render() {
    return this.props.todos.length === 0 ? (
      <Alert bsStyle="warning">
        <h5>
          <strong>Holy guacamole!</strong>
        </h5>{' '}
        There are no todos! Add some todos.
      </Alert>
    ) : null;
  }
}
