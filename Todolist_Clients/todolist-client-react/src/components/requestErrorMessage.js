import React from 'react';
import { Alert } from 'react-bootstrap';
import './requestErrorMessage.css';

export default class RequestErrorMessage extends React.Component {
  constructor(props, context) {
    super(props, context);
  }

  render() {
    return this.props.errorOnRequest ? (
      <Alert bsStyle="danger">
        <h5>
          <strong>Oops!</strong>
        </h5>{' '}
        An error has occurred. Try it again!
      </Alert>
    ) : null;
  }
}
