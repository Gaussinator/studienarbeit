import React from 'react';
import { ListGroup, ListGroupItem } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faMinusCircle } from '@fortawesome/free-solid-svg-icons';
import WarningMessage from './warningMessage';
import RequestErrorMessage from './requestErrorMessage';
import './editTodo.css';

export default class EditTodo extends React.Component {
  constructor(props, context) {
    super(props, context);
  }

  render() {
    const listItems = this.props.todos.map(todo => (
      <ListGroupItem key={todo.id} className="wrap-list">
        <span className="float left">{todo.description}</span>
        <FontAwesomeIcon
          className="delete-button float float-right"
          icon={faMinusCircle}
          onClick={() => {
            this.props.onClick(todo.id);
          }}
        />
      </ListGroupItem>
    ));

    return this.props.errorOnRequest === false ? (
      <div className="list">
        <WarningMessage todos={this.props.todos} />
        <ListGroup>{listItems}</ListGroup>
      </div>
    ) : (
      <RequestErrorMessage errorOnRequest={this.props.errorOnRequest} />
    );
  }
}
