import axios from 'axios';

export default class RequestSerivce {
  httpOptions = {
    headers: { 'Content-Type': 'application/json' }
  };

  constructor(serverUrl) {
    if (serverUrl) {
      this.serverUrl = serverUrl;
    } else {
      this.serverUrl = 'http://localhost:8080/';
    }
  }

  getCompleteTodoList() {
    return axios.get(this.serverUrl + 'api/todo/getCompleteList');
  }

  addTodoToTodoList(todo) {
    return axios.post(this.serverUrl + '/api/todo/add', todo, this.httpOptions);
  }

  removeTodo(id) {
    return axios.delete(this.serverUrl + `api/todo/delete/${id}`);
  }
}
