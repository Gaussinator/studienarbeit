# Studienarbeit - Ein verteiltes System mit Angular, React, Spring Boot und Docker

In diesem Repository befindet sich die komplette Studienarbeit mit dem Titel "Ein verteiltes System mit Angular, React, Spring Boot und Docker ".
Es befindet sich eine Dokumentation sowie die jeweiligen Komponenten in diesem Repository.

In dem Ordner "Todolist_Clients" befinden sich die unterschiedlichen Clients in Angular 7 sowie in React.js.

In dem Ordner "Todolist_Backend" befindet sich das Backend in Spring Boot.

**_ Angular und React Client lokal auf der Maschine bauen _**

Um den Client in Angular zu starten sind folgende Voraussetzungen notwendig:

    +	Angular benötigt Node.js version 8.x oder 10.x oder höher.

    	+	Hierfür empfiehlt es sich, eine aktuelle Version zu installieren.
    	+	Link zu Node.js ->	https://nodejs.org/en/

    +	Angular benötigt außerdem die Angular CLI

    	+	Um diese zu installieren folgendes im Terminal eingeben: npm install -g @angular/cli

Außerdem müssen die benötigten Abhängigkeiten für die Angular Anwendung installiert werden.
Dies geschieht wie folgt.

    +	cd \Todolist_Clients\todolist-client-angular	// Mittels Terminal zu dem Pfad in dem das Angular Projekt ist navigieren

    +	npm install				// Hiermit werden alle Abhängigkeiten, die in der package.json definiert sind, installiert.

Nachdem die Voraussetzungen erfüllt sind, kann man die Angular Anwendung wie folgt starten:

    +	cd \Todolist_Clients\todolist-client-angular	// Mittels Terminal zu dem Pfad in dem das Angular Projekt ist navigieren

    +	ng serve --open			// Diesen Befehl in dem zuvor Navigierten Pfad im Terminal eingeben.
    							// Anschließend öffnet sich der Browser mit localhost:4200.
    							// Beim weg lassen des Flags '--open' starten die Anwendung ohne dass sich der Browser öffnet.
    							// Falls der Port belegt sein sollte, lässt sich mittels folgendem Flag die Anwendung auf einem anderen Port starten.
    							// ng serve --port 8080
    							// In diesem Fall startet die Anwendung mit dem Port 8080.

Um die React Anwendung local zu starten und zu bauen ist ebenso Node.js notwendig, da dies bereits für Angular erklärt wurde, wird dies nicht mehr erklärt.

Folgende Schritte sind notwendig:

    +	cd \Todolist_Clients\todolist-client-react	// Mittels Terminal zu dem Pfad in dem das React Projekt ist navigieren

    + 	npm install 			// Hiermit werden alle Abhängigkeiten, die in der package.json definiert sind, installiert.

    +	npm start				// Damit wird die Anwendung gebaut und gestartet.

**_ Das verteilte System mittels Docker Compose starten _**

Um das verteilte System mittels Docker Compose zu starten und damit
die diversen Container mit ihrer jeweiligen Konfiguration zu starten, ist folgendes nötig:

    +   Docker muss installiert sein, da dieses normalerweiße Docker Compose mit installiert.

        + Ist das nicht der Fall, so muss Docker Compose nachträglich installiert werden

    +   Um nun das verteilte System zu starten, muss mittels CMD in das Projektverzeichnis navigiert werden,
        in der sich die docker-compose.yml Datei befindet.

        + Anschließend muss folgender Befehl in der CMD eingegeben werden

            + docker-compose up
