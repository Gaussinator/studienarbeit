package com.studienarbeit.todolist;

        import com.studienarbeit.todolist.model.Todo;
        import com.studienarbeit.todolist.model.TodoRepository;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.boot.SpringApplication;
        import org.springframework.boot.autoconfigure.SpringBootApplication;
        import org.springframework.http.HttpStatus;
        import org.springframework.http.ResponseEntity;
        import org.springframework.web.bind.annotation.*;

        import java.lang.reflect.Array;
        import java.util.ArrayList;

@SpringBootApplication
@RestController
@CrossOrigin
public class TodolistApplication {

    @Autowired
    TodoRepository todoRepository;

    private ArrayList<Todo> getTodoList() {
        ArrayList<Todo> todoList = new ArrayList<>();
        todoRepository.findAll().forEach((todo) -> {
            todoList.add(todo);
        });
        return todoList;
    }

    @CrossOrigin
    @RequestMapping(value = "/api/todo/getCompleteList", method = RequestMethod.GET)
    public ResponseEntity<ArrayList<Todo>> getCompleteTodolist() {
        try {
            ArrayList<Todo> todoList = this.getTodoList();
            return new ResponseEntity<>(todoList, HttpStatus.OK);
    }
        catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @CrossOrigin
    @RequestMapping(value = "/api/todo/add", method = RequestMethod.POST)
    public ResponseEntity<Void> addTodo(@RequestBody String todo) {
        try {
            if (todo != null && todo != "") {
                todoRepository.save(new Todo(todo));
                System.out.println("The todo with the description: '" + todo + "' was added.");
                return new ResponseEntity<>(HttpStatus.CREATED);
            }
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @CrossOrigin
    @RequestMapping(value = "/api/todo/delete/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Void> removeTodo(@PathVariable("id") int id) {
        try {
                if (todoRepository.existsById(id)) {
                    todoRepository.deleteById(id);
                    System.out.println("The todo with the ID: " + id + " was deleted." );
                    return new ResponseEntity<>(HttpStatus.OK);
                }
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        catch(Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public static void main(String[] args) {
        SpringApplication.run(TodolistApplication.class, args);
    }
}
